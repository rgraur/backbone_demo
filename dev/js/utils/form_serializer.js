/**
 * Serialize forms (jQuery objects).
 *
 * @param  {Object} $ jQuery library
 */
define(['jquery'], function($){
  return function(){
    /**
     * Serialize form into object.
     *
     * @param  {Object} $form jQuery form object
     * @return {Object}       serialized form
     */
    this.serializeObject = function($form)
    {
      // chec param
      if(typeof($form) === 'undefined' || !$form.length)
      {
        throw "Invalid form";
      }

      // serialize as array
      var obj = {};
      var arr = $form.serializeArray();

      // convert array to object
      $.each(arr, function(){
        if (obj[this.name] !== undefined) {
          if (!obj[this.name].push) {
            obj[this.name] = [obj[this.name]];
          }

          obj[this.name].push(this.value || '');
        } else {
          obj[this.name] = this.value || '';
        }
      });

      return obj;
    };
  };
});