/**
 * Backbone router.
 *
 * @param  {Object} Backbone backbone library
 * @return {Object}          backbone router
 */
define(['backbone'], function(Backbone){
  return Backbone.Router.extend({
    // page routes
    routes: {
      '':         'users',      // list
      'new':      'editUser',   // create
      'edit/:id': 'editUser'    // edit
    }
  });
});