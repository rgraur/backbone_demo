/**
 * Returns users Backbone collection.
 *
 * @param  {Object} Backbone backbone library
 * @return {Object} backbone collection of users
 */
define(['backbone'], function(Backbone){
  return Backbone.Collection.extend({
    /**
     * Server URL to call.
     *
     * @type {String}
     */
    url: '/users',

    /**
     * Parse request response and escape values for template.
     *
     * @param  {Array} users users returned from server
     * @return {Array}       escaped users
     */
    parse: function(users){
      _.each(users, function(user, index){
        _.each(user, function(val, key){
          users[index][key] = _.escape(val);
        });
      });

      return users;
    }
  });
});