"use strict"

// paths to dependencies
require.config({
  paths: {
    jquery:     'vendor/jquery/jquery',
    underscore: 'vendor/underscore-amd/underscore',
    backbone:   'vendor/backbone-amd/backbone'
  }
});

/**
 * Hook to add external domain to URL on ajax requests.
 */
require(['jquery'], function($){
  $.ajaxPrefilter(function(options, originalOptions, jqXHR){
    options.url = 'http://backbonejs-beginner.herokuapp.com' + options.url;
  });
});

/**
 * Load Backbone router.
 *
 * @param {Object} Backbone backbone library
 * @param {Object} Router   backbone router
 * @param {Object} UserList backbone view for user list
 */
require(
  ['backbone', 'utils/router', 'views/user_list', 'views/edit_user'],
  function(Backbone, Router, UserListView, EditUserView){
    var userListView = null;
    var editUserView = null;

    // route listeners
    var router = new Router();
    router
    .on('route:users', function(){
      // instantiate view singleton
      if(userListView === null){
        // inject global router to navigate
        userListView = new UserListView({router: router});
      }

      // render user list view
      userListView.render();
    })
    .on('route:editUser', function(id){
      // instantiate view singleton
      if(editUserView === null){
        // inject global router to navigate
        editUserView = new EditUserView({router: router});
      }

      // render edit user view
      editUserView.render({id: id});
    });

    // start listening the routes
    Backbone.history.start();
  }
);
