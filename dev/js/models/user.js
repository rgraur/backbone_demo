/**
 * User model.
 *
 * @param  {Object} Backbone backbone library
 * @return {Object} backbone model for user
 */
define(['backbone'], function(Backbone){
  return Backbone.Model.extend({
    urlRoot: '/users',

    /**
     * Validate attributes before save.
     *
     * @param  {Object} attributes model attributes
     * @return {String}            failure message
     */
    validate: function(attributes){
      if(attributes.age <= 0 || attributes.firstname === '' || attributes.lastname === ''){
        return 'Invalid attributes!';
      }
    },

    /**
     * Constructor.
     *
     * Listen for invalid attributes on save or set.
     */
    initialize: function(){
      this.on('invalid', function(model, error){
        // validation failed
        alert(error);
      });
    }
  });
});