/**
 * Returns edit user Backbone view.
 *
 * @param  {Object} Backbone        backbone library
 * @param  {Object} FormSerializer  form serializer library to convert form to object
 * @param  {Object} UserModel       backbone model for user
 * @return {Object}                 backbone view for edit user
 */
define(
  ['backbone', 'utils/form_serializer', 'models/user'],
  function(Backbone, FormSerializer, UserModel, UsersCollection){
    return Backbone.View.extend({
      el: '.page',

      defaults: {
        router: null
      },

      /**
       * Constructor.
       *
       * Extends defaults.
       *
       * @param  {Object} options will extend defauls
       */
      initialize: function(options){
        _.extend(this.defaults, options);
      },

      /**
       * Render create/edit user template.
       */
      render: function(options){
        // update
        if(options.id){
          // element cache for success method
          var $el = this.$el;

          // populate form from model (model is attached to view to be available on delete action)
          this.userModel = new UserModel({id: options.id});

          // GET /users/:id
          this.userModel.fetch({
            success: function(user){
              // no user
              if(!user){
                return;
              }

              // populate Underscore JS template with user data
              var template = _.template(
                $('#edit_user_template').html(), // template element
                {user: user}                     // model
              );

              // render template into page
              $el.html(template);
            }
          });
        // create
        }else{
          var template = _.template(
            $('#edit_user_template').html(), // template element
            {user: null}                     // no model on create
          );

          this.$el.html(template);
        }
      },

      /**
       * Form event handlers.
       */
      events: {
        'submit .edit-user-form': 'saveUser',
        'click .delete': 'deleteUser'
      },

      /**
       * Save user on form submit.
       *
       * @param  {Object} event triggered event
       */
      saveUser: function(event){
        // serialize form to object
        var formSerializer = new FormSerializer();
        var formObj = formSerializer.serializeObject($(event.currentTarget));
        var userModel = new UserModel();

        var thisView = this;
        // POST (on create), PUT (on update) /users
        userModel.save(formObj, {
          success: function(user){
            // go back to users list
            thisView.defaults.router.navigate('', {trigger: true});
          }
        });

        event.stopImmediatePropagation();
        return false;
      },

      /**
       * Delete user.
       */
      deleteUser: function(event){
        var thisView = this;
        // DELEETE /user/:id
        this.userModel.destroy({
          success: function(){
            // go back to users list
            thisView.defaults.router.navigate('', {trigger: true});
          }
        });

        event.stopImmediatePropagation();
        return false;
      }
    });
  }
);