/**
 * Returns users list Backbone view.
 *
 * @param  {Object} Users     backbone collection of users
 * @param  {Object} UserModel backbone model of user
 * @return {Object}           backbone view of user list
 */
define(['collections/users', 'models/user'], function(UsersCollection, UserModel){
  return Backbone.View.extend({
    el: '.page',

    defaults: {
      router: null
    },

    /**
     * Constructor.
     *
     * Extends defaults.
     *
     * @param  {Object} options will extend defauls
     */
    initialize: function(options){
      _.extend(this.defaults, options);
    },

    /**
     * Form event handlers.
     */
    events: {
      'click .delete-row': 'deleteUser'
    },

    /**
     * Render user list template and populate with users.
     */
    render: function(){
      // element cache
      var $el = this.$el;

      // fetch returned users
      var usersCollection = new UsersCollection();

      // GET /users
      usersCollection.fetch({
        /**
         * Populate template with users on success.
         *
         * @param  {Object} users backbone collection of users
         */
        success: function(users){
          // no users
          if(!users){
            $('#users').replace('No users found!');

            return;
          }

          // populate Underscore JS template with users
          var template = _.template(
            $('#user_list_template').html(), // template element
            {
              users: users.models            // template data
            }
          );

          // render template into page
          $el.html(template);
        }
      });
    },

    /**
     * Delete user.
     */
    deleteUser: function(event){
      var userModel = new UserModel({id: $(event.currentTarget).data('id')});

      var thisView = this;
      // DELEETE /user/:id
      userModel.destroy({
        success: function(){
          // refresh (workaround)
          var _tmp = Backbone.history.fragment;
          thisView.defaults.router.navigate(_tmp + (new Date).getTime());
          thisView.defaults.router.navigate(_tmp, {trigger:true});
        }
      });

      event.stopImmediatePropagation();
      return false;
    }
  });
});