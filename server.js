/**
 * This is a HTTP web server using NodeJS Connect module.
 *
 * How to run the server:
 * $ node server.js
 */

var connect = require('connect');
var path    = __dirname + '/dev';
var port    = 8080;

console.log('Running NodeJS Connect HTTP web server...');
console.log('Root path: ' + path);
console.log('Port: ' + port);
console.log('URL: http://localhost:' + port);

connect.createServer(connect.static(path)).listen(port);
