({
  appDir:         'dev/',
  baseUrl:        'js/',
  dir:            'prod/',
  mainConfigFile: 'dev/js/main.js',
  name:           'main',
  optimizeCss:    'standard'
})